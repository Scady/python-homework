number = ""
other_number = ""

number = int(input("Пожалуйста введите число: "))
print(number)

while(number != 0):
    if(number >= 1000):
        number = number - 1000
        other_number = other_number + "M"
    elif(number >= 900):
        number = number - 900
        other_number = other_number + "CM"
    elif(number >= 500):
        number = number - 500
        other_number = other_number + "D"
    elif(number >= 400):
        number = number - 400
        other_number = other_number + "CD"
    elif(number >= 100):
        number = number - 100
        other_number = other_number + "C"
    elif(number >= 90):
        number = number - 90
        other_number = other_number + "XC"
    elif(number >= 50):
        number = number - 50
        other_number = other_number + "L"
    elif(number >= 40):
        number = number - 40
        other_number = other_number + "XL"
    elif (number >= 10):
        number = number - 10
        other_number = other_number + "X" 
    elif (number >= 9):
        number = number - 9
        other_number = other_number + "IX" 
    elif (number >= 5):
        number = number - 5
        other_number = other_number + "V" 
    elif (number >= 4):
        number = number - 4
        other_number = other_number + "IV" 
    else:
        number = number - 1
        other_number = other_number + "I" 
        
print("Отформатированное число: " + other_number)
