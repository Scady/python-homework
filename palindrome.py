string = ""
one_string = ""
two_string = ""

while(True):
    string = input("Введите строку: ")

    for i in range(0, len(string)):
        one_string = one_string + string[i].lower()

    for i in reversed(range(0, len(string))):
        two_string = two_string + string[i].lower()
    
    if(one_string == two_string):
        print("Эта строка является палиндромом!")
    else:
        print("Эта строка НЕ является палиндромом!")